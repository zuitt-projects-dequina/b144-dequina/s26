const http = require('http')

//create a variable "port" to store the port number
const port = 4000

// create a variable "server" that stores the output of the 'create server' method
const server = http.createServer((req, res) => {
	//http://localhost:4000/greeting
	if (req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Hello World')
	}
	//http://localhost:4000/homepage
	if (req.url == '/homepage') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This is Homepage')
	}
	else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page Not Found')
	}
})

// Use the 'server' and 'port' variables created above
server.listen(port);

// when server is running, console will print the message:
console.log(`Server now accessible at localhost: ${port}`)