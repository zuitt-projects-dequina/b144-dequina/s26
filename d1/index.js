// Node.js Introduction

/*
Objectives:

	- understand Client-Server architecture
	- use Node.js in creating a web server
	- use route handling to perform diff actions based on which URL endpoint is accessed; this is where PHP enters

__________________________
Client-Server Architecture

	At this point:
		1. dev a static front-end app (static means, no DATA, more on information)
		2. create and manipulate a MongoDB Database


	How to make a "dynamic content"
		- dynamic means, retrieve, collect data to manipulate

	Client-Server Archictecture: 
		Clients (phones, PC) --> Internet --> Server

		[		 			] 	----request-->	[     	 ]  
		[ Client 	]										[ Server ]
		[		 			]		<--response--		[        ]

		server wont make any response if no request received

	Benefits:
		1. centralized data makes apps more scalable and maintainable
		2. multiple client apps may all use dynamically-generated data
		3. workload is concentrated on the server, makeing clients apps lightweight
			- front-end is lightweight, because all data gathering is on the back-end
	
________________	
What is Node.js?
	- an open-source, JavaScript runtime environment for creating server-side applications

	Runtime Environment
		- gives the context for running a programming language
		- JS  was initially within the context of the browser, gitving it access to:
				DOM, window object, etc
		- with Node.js the context was taken out of the browser and put into the server
		- with Node.js, JS now has access to the ff:
				- systyem resources
				- memory
				- file system
				- input/output (I/O)
				- network

	Benefits
		- Performance
			- optimized for Web applications
		- Familiarity
			- "same old" JavaScript
		- access to  node package manager (NPM)
			- worlds largest packages

*/

//_______________________________________________
// use the required directive to load Node.js Mdules
// "module" is a software component or part of a program that contains one or more routines
// "http module" - lets Node.js trasfer data using the hypertext trasfer protocol. It is  set of individual files that contain code to create a "component" that helps establish data transfer between applications
// hypertext transfer protocol; allows us to fetch resources such as HTML 

let http = require("http");

/*
___________________
Why do we use HTTP?
-------------------

	- we have clients (browser) and a server (nodeJs/ExpressJS application) communicate by exchanging individual messages
	- message sent by client, usually a web browser are called "REQUESTS"
	- http://home (this is the server is expecting)
	- the message sent by the server as an answer are called responses
*/


/*
 createServer()	--> creates an "http server" that "listens" to requests from client
				--> it accepts a function and allows us to perform a certain task to our server

		syntax: 

				http.createServer( function(<request>, <responce>) {
		
						<statement/s>

				}).listen(<port>)


		// port
			- is a virtual point where network connections start and end.
			- each port is associated with a specific process or service
			- the server will be assigned to port 4000 via the "listen4000" method where the server will listen to any requests that are sent to our server
		// in Terminal: netstat -a   --> this is to know what addresses are running on your local machine
*/

	// @terminal: node <filename> (to connect the js file to the browser) and (crtl + C) to stop the run
	// http://localhost:4000  (this is where "requests" are sent)

	// NOTE: parameters must be same, if wrong spelling... program wont connect

	// status that starts with 4xx the problem is on the client side; but 5xx status, problem on back-end
	/* 	Informational responses (100–199)
		Successful responses (200–299)
		Redirection messages (300–399)
		Client error responses (400–499)
		Server error responses (500–599)
	*/

http.createServer( function(request, response) {

  //use the writeHead() method to:
  	  // set a status code for the reponse - 200 means OK(succesful)
  // set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'}); // if database, because of JSON file, 'application/json'
	// we use the reponse.end() method to end the response process
	response.end('Hello World');

}).listen(4000)

// when a server is running, console will print this message
console.log('Server running at localhost:4000');


// What directive is used by Node.js in loading the modules it needs?

		require()

// What Node.js module contains a method for server creation?

		http module


// What is the method of the http object responsible for creating a server using Node.js?

		 createServer()


// What method of the response object allows us to set status codes and content types?

		writeHead()


// Where will console.log() output its contents when run in Node.js?

		gitbash or terminal

// What property of the request object contains the address's endpoint?

		URL 